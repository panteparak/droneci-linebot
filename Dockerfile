FROM python:3.5
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
VOLUME /app
EXPOSE 5000
CMD python app.py -p 5000 -d True
